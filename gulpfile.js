/**
 * Created by xiaoma on 2016/10/10.
 */
var gulp = require('gulp'),
    clean = require('del'),//clean build directory before rebuild
    sass=require('gulp-sass'),//sass to css
    csslint = require('gulp-csslint'),//validate css
    jshint = require('gulp-jshint'),//validate js
    uglifyJs = require('gulp-uglify'),//compress js
    minifyCss = require('gulp-clean-css');//compress css
//清空build文件夹
gulp.task('clean',function (cb) {
    clean(['build'], cb);
});

//压缩js到build目录下
gulp.task('uglify', function () {
    gulp.src('src/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(uglifyJs())
        .pipe(gulp.dest('build'));
});
//scss编译成css
gulp.task('sass',function () {
   gulp.src('src/**/*.scss')
       .pipe(sass().on('error',sass.logError))
       .pipe(gulp.dest('src'));
});
//压缩css到build目录下
gulp.task('cssmin',['sass'], function () {
    gulp.src('src/**/*.css')
        .pipe(csslint())
        .pipe(minifyCss())
        .pipe(gulp.dest('build'));
});
//监控scss文件变化，并自动编译
gulp.task('watch',function () {
    gulp.watch('src/**/*.scss',['sass']);
});
gulp.task('default', function () {
    gulp.start('clean','uglify', 'cssmin')
});