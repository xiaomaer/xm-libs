/**
 * Created by xiaoma on 2017/2/7.
 */
;(function (win, xm) {
    /**
     *
     * @param options:对象，格式如下：
     * photos支持传入json和直接读取页面图片两种方式，注意json格式必须严格按照下面的要求哦
     * {
     *    photos:{
     *       start:0,//初始显示的图片序号，默认0
     *       data:[{//相册包含的图片，数组格式
     *          src:"图片地址",
     *          alt:"图片描述"//选填
     *       }]
     *    }
     * }
     * 或者
     * {
     *    photos:'#photos-parent-elem'//存储图片的父容器
     * }
     */
    xm.photos = function (options) {
        var params = options.photos;
        if (!params) return;
        var photos_type = typeof params,
            start = options.start || 0,
            curr_index = start + 1,
            photos = params.data || [],
            photos_html, img_len;
        if (photos_type === 'string') {
            var images = document.querySelector(param).querySelectorAll('img');
            Array.forEach.call(images, function () {
                photos.push({
                    src: this.src,
                    alt: this.alt
                });
            });
        }
        img_len = photos.length;
        if (img_len === 0) return;
        photos_html = '<div class="XM-photos-container"></div>' +
            '<div class="XM-show-photos">' +
            '<img src="' + photos[start].src + '" alt="' + photos[start].alt + '" class="show-image"/>' +
            '<div class="arr arr-left"></div><div class="arr arr-right"></div>' +
            '<div class="photo-info">' +
            '<span class="photo-title">' + photos[start].alt + '</span>' +
            '<span class="photo-index">' + curr_index + "/" + img_len + '</span>' +
            '</div>';
        document.body.insertAdjacentHTML('beforeend', photos_html);
        var photos_container = document.querySelector('.XM-show-photos'),
            show_image = photos_container.querySelector('.show-image'),
            photo_title = photos_container.querySelector('.photo-title'),
            photo_index = photos_container.querySelector('.photo-index'),
            arr_left = photos_container.querySelector('.arr-left'),
            arr_right = photos_container.querySelector('.arr-right');
        //点击图片外的区域关闭图片展示层
        document.querySelector('.XM-photos-container').addEventListener('click', function (e) {
            var parent_elem = this.parentNode;
            parent_elem.removeChild(this);
            parent_elem.removeChild(photos_container);

        }, false);
        //上一张
        arr_left.addEventListener('click', function (e) {
            e.stopPropagation();
            if (start === 0) {
                start = img_len - 1;
            } else {
                start--;
            }
            curr_index = start + 1;
            show_image.src = photos[start].src;
            show_image.alt = photos[start].alt;
            photo_title.innerHTML = photos[start].alt;
            photo_index.innerHTML = curr_index + '/' + img_len;
        }, false);
        //下一张
        arr_right.addEventListener('click', function (e) {
            e.stopPropagation();
            if (start === (img_len - 1)) {
                start = 0;
            } else {
                start++;
            }
            curr_index = start + 1;
            show_image.src = photos[start].src;
            show_image.alt = photos[start].alt;
            photo_title.innerHTML = photos[start].alt;
            photo_index.innerHTML = curr_index + '/' + img_len;
        });
    };
})(window, window['xm'] || (window['xm'] = {}));