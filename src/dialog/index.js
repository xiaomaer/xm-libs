/**
 * Created by xiaoma on 2016/10/10.
 */
//弹层对话框：alert、confirm
/*alert对话框options参数说明：
 * title:标题，必填写
 * btns:按钮，选填写，默认值为我知道了，点击按钮，关闭对话框*/

/*var options = {
 title: '我是标题',
 btns: ['我知道了']
 };*/

/*confirm对话框options参数说明：
 * title:标题，必填写
 * btns:按钮，默认第一个按钮为取消，第二个按钮为确定，
 * 取消按钮callback默认为关闭对话框
 * 确定按钮callback没有默认操作，必须自己写哦*/

/*var options = {
 title: '我是标题',
 btns: [{
 btn: '取消',
 callback: function () {//选填

 }
 }, {
 btn: '确定',
 callback: function () {//必填

 }
 }]
 };*/

;(function (win, xm) {
    var mask_html = '<div id="XM-mask"></div>',
        mask_elem, dialog_elem,
        hideDialog = function () {
            var body_elem = document.body;
            body_elem.removeChild(mask_elem);
            body_elem.removeChild(dialog_elem);
        };

    //alert弹窗
    xm.alert = function (options) {
        var btn = options.btns || ['我知道了'],
            alert_html = '<div id="XM-dialog"><div class="title">' + options.title +
                '</div><div class="btns"><a href="#" class="btn btn-know">' + btn +
                '</a></div></div>',
            dialog_html = mask_html + alert_html;
        document.body.insertAdjacentHTML('beforeend', dialog_html);
        mask_elem = document.querySelector('#XM-mask');
        dialog_elem = document.querySelector('#XM-dialog');
        //点击按钮关闭对话框
        document.querySelector('.btn-know').addEventListener('click', hideDialog, false);
    };
    //confirm弹窗
    xm.confirm = function (options) {
        var btn_one = options.btns[0].btn || '取消',
            btn_two = options.btns[1].btn || '确定',
            confirm_html = '<div id="XM-dialog"><div class="title">' + options.title +
                '</div><div class="btns"><a href="#" class="btn btn-cancel">' + btn_one +
                '</a><a href="#" class="btn btn-ok">' + btn_two +
                '</a></div></div>',
            dialog_html = mask_html + confirm_html;
        document.body.insertAdjacentHTML('beforeend', dialog_html);
        mask_elem = document.querySelector('#XM-mask');
        dialog_elem = document.querySelector('#XM-dialog');
        //取消
        document.querySelector('.btn-cancel').addEventListener('click', function () {
            if (options.btns[0].callback) {
                options.btns[0].callback();
            } else {
                hideDialog();
            }
        }, false);
        //确定
        document.querySelector('.btn-ok').addEventListener('click', function () {
            if (options.btns[1].callback) {
                hideDialog();
                //执行操作
                options.btns[1].callback();
            }
        }, false);
    };

})(window, window['xm'] || (window['xm'] = {}));