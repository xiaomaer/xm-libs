/**
 * Created by xiaoma on 2017/2/28.
 */
/**
 *下拉加载更多实现原理：
 * 监听scroll事件，当文档总高度（document.body.clientHeight）-(窗口可视区域的高度(window.innerHeight)+页面向上滚动的高度(scrollTop))<distance时，
 * 通过回调函数，加载更多数据进行渲染。
 *
 * 注意点1：文档总高度-distance<窗口可视区域高度时，直接自动加载数据进行渲染页面
 * 注意点2：当请求未完成时，即使滚动满足加载更多数据的条件，也不可以再发送请求，必须等当前请求完成，通过isLoading属性进行控制
 * 注意点3：如果页面已经显示了所有数据，下方显示"已显示全部数据"提示信息，通过hasData属性进行控制
 * 注意点4：每次请求完成，渲染页面后，要对isLoading和下拉提示信息进行重置，重新获取文档总高度
 */
;(function (win, xm) {
    var windowHeight = win.innerHeight || document.documentElement.clientHeight,//窗口可视区域高度
        pageHeight = document.body.clientHeight,//文档总高度
        html = {
            refresh: '<div class="J_refresh">上拉加载更多</div>',
            load: '<div class="J_loading">加载中<span class="dot">.</span></div>',
            noData: '<div class="J_noMore">已显示全部数据</div>'
        };
    xm.pullLoadMore = {
        /**
         *
         * @param options:对象
         * options={
         *    element:加载更多元素显示区域，即渲染后dom结构插入的父级元素。默认插入在document.body元素开始之后。
         *    distance:'距底部的距离为小于多少是加载更多'，默认值为20。
         *    callback：上拉加载更多执行的操作。
         * }
         */
        init: function (options) {
            var that = this,
                elem = options.element;
            //要加载的元素下方，插入loading提示
            elem && elem.insertAdjacentHTML('afterend', '<div class="J_remind">' + html.refresh + '</div>');
            !elem && document.body.insertAdjacentHTML('beforeend', '<div class="J_remind">' + html.refresh + '</div>');
            that.remind = document.querySelector('.J_remind');
            that.isLoading=false;//用于判断是否loading状态，解决正在loading时，滚动符合条件时仍发送请求加载更多
            that.hasData=true;//是否已显示全部数据，true表示没有显示全部数据
            //其他函数可以访问
            that.distance = options.distance || 20;
            that.callback = options.callback || '';
            //当文档高度小于窗口高度时，自动加载数据
            that._autoLoad();
            //滚动加载更多数据
            win.addEventListener('scroll', function (e) {
                var scrollTop = e.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop,//页面纵向滚动距离
                    dis = pageHeight - windowHeight - scrollTop;
                if (dis < that.distance&&!that.isLoading&&that.hasData) {
                    that._loadMore();
                }
            });
        },
        //每请求完成一次都要调用该函数进行重置
        reset: function () {
            //重置滚动loading状态
            this.isLoading=false;
            if(this.hasData){
                //重置loading提示信息
                this.remind.innerHTML = "";
                this.remind.insertAdjacentHTML('afterbegin', html.refresh);
                //重新文档总高度
                pageHeight = document.body.clientHeight;
                this._autoLoad();
            }
        },
        noMore: function () {
            //当加载完成所有数据时调用
            this.remind.innerHTML = "";
            this.remind.insertAdjacentHTML('afterbegin', html.noData);
            //已显示全部数据
            this.hasData=false;
        },
        _autoLoad: function () {
            //如果文档高度不大于窗口高度，数据较少，自动加载下方数据
            if ((pageHeight - this.distance) < windowHeight) {
                this._loadMore();
            }
        },
        _loadMore: function () {
            this.remind.innerHTML = "";
            this.remind.insertAdjacentHTML('afterbegin', html.load);
            this.isLoading=true;
            this.callback && this.callback();
        }
    };

})(window, window['xm'] || (window['xm'] = {}));