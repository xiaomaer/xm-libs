/**
 * Created by xiaoma on 2016/10/10.
 */
//toast提示框
;(function (win, xm) {
    var elemNode, timer;
    /*弹出提示
     *options:对象，{message:'',duration:''}
     * message：必填，toast显示的提示消息。
     * duration：选填，提示框消失的时间，毫秒为单位，默认值为1500ms。*/
    xm.toast = function (options) {
        var msg = options.message || '',
            duration = options.duration || 1500;
        //如果页面有toast显示时，再显示另外一个toast，要先关闭timer,不然toast小时时间不对
        if (timer) {
            clearInterval(timer);
        }
        if (!elemNode) {
            elemNode = document.createElement('div');
            document.body.appendChild(elemNode);
        }
        elemNode.innerHTML = msg;
        elemNode.className = 'toast-show';
        timer = setTimeout(function () {
            elemNode.className = 'toast-show toast-hide';
        }, duration);
    };
})(window, window['xm'] || (window['xm'] = {}));

