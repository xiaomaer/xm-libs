/**
 * Created by xiaoma on 2017/2/20.
 * 思路：通过touch事件计算下拉距离，当下拉距离大于给定值时，显示loading图标，并且loading图标也向下跟着移动该下拉距离；
 * 下拉完成后，即触发touchend事件时，通过回调函数执行对应操作，然后调用hide方法移除loading图标。
 */
;(function (win, xm) {
    var doc = document,
        startX, startY, endX, endY, offsetX, offsetY,
        loading_html = '<div id="XM-loading">.</div>',
        loading_elem, loading_height = 60,
        handleTouchMove = function (options) {
            var element = options.element,
                distance = options.distance || 50;
            if (offsetY > offsetX && offsetY > distance) {
                if (!loading_elem) {
                    element && element.insertAdjacentHTML('beforebegin', loading_html);
                    !element && doc.body.insertAdjacentHTML('afterbegin', loading_html);
                }
                loading_elem = doc.querySelector('#XM-loading');
                //下拉时，loading高度跟着增加对应的下拉距离
                loading_elem.style.height = (loading_height + offsetY) + 'px';
            }
        },
        handleTouchEnd = function (options) {
            var callback = options.callback;
            if (loading_elem) {
                //下拉完成时，loading高度还原到原来的高度
                loading_elem.style.height = loading_height + 'px';
                //执行刷新页面的相应操作
                callback && callback();
            }
        };

    xm.pullRefresh = {
        /**
         *
         * @param options:object
         * options={
         *    element:loading插入的位置，即'把loading元素插入到改元素位置之前',默认插入在document.body元素开始之后。
         *    distance:'下拉的距离'，默认值为50。
         *    callback：下拉刷新执行的操作。
         * }
         */
        init: function (options) {
            doc.addEventListener('touchstart', function (e) {
                var touches = e.targetTouches;
                startX = touches[0].pageX;
                startY = touches[0].pageY;
            }, false);
            doc.addEventListener('touchmove', function (e) {
                var touches = e.touches;
                if (touches.length === 1) {
                    endX = touches[0].pageX;
                    endY = touches[0].pageY;
                    offsetX = endX - startX;
                    offsetY = endY - startY;
                    handleTouchMove(options);
                }
            }, false);
            doc.addEventListener('touchend', function (e) {
                e.preventDefault();
                handleTouchEnd(options);
            });
        },
        hide: function () {
            loading_elem.classList.add('hide');
            loading_elem.parentNode.removeChild(loading_elem);
            loading_elem="";
        }
    };
})(window, window.xm || (window.xm = {}));