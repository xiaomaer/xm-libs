/**
 * Created by xiaoma on 2017/2/4.
 */
;(function (win, xm) {
    var tips,
        getOffsetTop = function (elem) {
            var top = elem.offsetTop,
                parent = elem.offsetParent;
            while (parent !== null) {
                top += parent.offsetTop;
                parent = parent.offsetParent;
            }
            return top;
        },
        getOffsetLeft = function (elem) {
            var left = elem.offsetLeft,
                parent = elem.offsetParent;
            while (parent !== null) {
                left += parent.offsetLeft;
                parent = parent.offsetParent;
            }
            return left;
        };
    xm.tips = {
        /*options参数配置：
         * text:必填，提示信息
         * follow：必填，tips依附元素，格式：document.querySelector()方法支持的选择器
         * position:选填，tips相对于follow元素的位置，默认在上方，值包括：top、bottom、left和right
         * bgcolor:选填，tips背景色，默认背景色为蓝色，支持所有的css颜色值
         * color：选填，tips字体颜色，默认字体颜色为白色，支持所有的css颜色值
         */
        show: function (options) {
            var tips_html = '<div class="XM-tips">' + options.text + '<div class="triangle"></div></div>',
                f_elem = document.querySelector(options.follow),
                f_top = getOffsetTop(f_elem),
                f_left = getOffsetLeft(f_elem),
                f_height = f_elem.offsetHeight,
                f_width = f_elem.offsetWidth,
                distance = 10,
                position = options.position || 'top',
                bgcolor = options.bgcolor||'#40AFFE',
                color = options.color,
                tips_height, tips_width, left, top, position_css = "", triangle;
            document.body.insertAdjacentHTML('beforeend', tips_html);
            tips = document.querySelector('.XM-tips');
            triangle = document.querySelector('.triangle');
            tips_height = tips.offsetHeight;//height+padding+border
            tips_width = tips.offsetWidth;
            if (color) {
                position_css += "color:" + color + ";";
            }
            switch (position) {
                case 'top':
                    left = f_left + "px";
                    top = f_top - distance - tips_height + 'px';
                    triangle.style.borderColor = bgcolor + " " + bgcolor + " transparent transparent";
                    triangle.style.cssText += "left:10px;bottom:-10px;";
                    break;
                case 'bottom':
                    left = f_left + "px";
                    top = f_top + f_height + distance + "px";
                    triangle.style.borderColor = "transparent transparent "+bgcolor + " " + bgcolor;
                    triangle.style.cssText += "left:10px;top:-10px;";
                    break;
                case 'left':
                    left = f_left - distance - tips_width + 'px';
                    top = f_top + "px";
                    triangle.style.borderColor = "transparent transparent "+bgcolor + " " + bgcolor;
                    triangle.style.cssText += "right:-10px;top:5px;";
                    break;
                case 'right':
                    left = f_left + f_width + distance + "px";
                    top = f_top + "px";
                    triangle.style.borderColor = bgcolor + " " + bgcolor + " transparent transparent";
                    triangle.style.cssText += "left:-10px;top:5px";
                    break;
            }
            position_css +="background-color:" + bgcolor + ";left:" + left + ";top:" + top;
            tips.style.cssText = position_css;
        },
        hide: function () {
            tips.classList.add('hide');
            setTimeout(function () {
                tips && document.body.removeChild(tips);
            }, 500);
        }
    };

})(window, window['xm'] || (window['xm'] = {}));